import java.util.Date;


public class Sorteio {
    private int id;
    private int ano;
    private int identificador;
    private double valorPremio;
    private int apresentador;
    private int auditor;
    private int premiado;
    private String descricao;
    private String tipo;
}

public class Usuario {
    private int id;
    private Date dataNascimento;
    private String nome;
    private String cpf;
    private String email;
    private String perfil;
    private String role;
    private String senha;
    private String telefone;
    private String estado;
    private String municipio;
    private String cep;
    private String bairro;
    private String logradouro;
    private String numero;
    private String complemento;
}

public class Bilhete {
    private int id;
    private int ano;
    private int mes;
    private int nota_fiscal_id;
    private int usuario_id;
}
 
public class NotaFiscal {
    private Double id;
    private Double usuario_id;
    private String chave;
}

public class Exercicio0605 {
    public static void main(String[] args) {
      
    }
}
